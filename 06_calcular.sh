#!/bin/bash

# Solicitação ao usuário para digitar um número inteiro
echo "Digite um número inteiro:"
read x

# Calculo do valor de y
y=$((x*x + 7))

# Impressão dos valores de x e y
echo "O valor de x é: $x"
echo "O valor de y é: $y"
