#!/bin/bash

# Criando variáveis no Bash
# Forma 1: Atribuição direta
minha_variavel="Isso é uma variável."

# Forma 2: Lendo um valor do usuário
echo "Digite o valor da variável:"
read variavel_usuario

# Forma 3: Recebendo parâmetro da linha de comando
# Suponha que este script seja chamado como ./script.sh parametro_da_linha_de_comando
parametro_linha_de_comando="$1"

# Exibindo os valores das variáveis criadas
echo "Forma 1: $minha_variavel"
echo "Forma 2: $variavel_usuario"
echo "Forma 3: $parametro_linha_de_comando"

# Diferença entre leitura do usuário e parâmetro da linha de comando
echo "Diferença entre leitura do usuário e parâmetro da linha de comando:"
echo "Quando pedimos ao usuário para digitar um valor, podemos obter entrada em tempo de execução."
echo "Quando recebemos um parâmetro da linha de comando, o valor já está definido antes da execução do script."

# Variáveis automáticas
echo "Variáveis automáticas:"
echo "As variáveis automáticas são variáveis internas do Bash que armazenam informações sobre a execução do script."
echo "Alguns exemplos comuns:"
echo "1. \$0: Nome do script - $0"
echo "2. \$#: Número de argumentos passados - $#"
echo "3. \$@: Lista de todos os argumentos - $@"
echo "4. \$1, \$2, ...: Argumentos individuais - \$1=$1, \$2=$2"

# Exemplo de uso de variáveis automáticas
echo "Exemplo de uso de variáveis automáticas:"
echo "Este script foi chamado com $# argumentos."
echo "Os argumentos passados foram: \$@"

