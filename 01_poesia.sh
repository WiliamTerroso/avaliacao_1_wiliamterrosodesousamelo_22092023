#! /bin/bash
echo -e "\e[1;36m Caderno de poesias,  \e[0m"
sleep 1
echo -e "\e[1;32m Caderno de poesias \e[0m"
sleep 1
echo -e "\e[1;31m é um belo lugar.  \e[0m"
sleep 1
echo -e "\e[1;33m Tantas coisas lindas. \e[0m"
sleep 1
echo -e "\e[1;34m que eu gostaria de falar. \e[0m"
sleep 1
echo -e "\e[1;36m Eu falo em forma de versos   \e[0m"
sleep 1
echo -e "\e[1;32m para todos poderem escutar. \e[0m"
sleep 1
echo -e "\e[1;33m Agora você já sabe \e[0m"
sleep 1
echo -e "\e[1;34m por que os poetas passam os dias  \e[0m"
sleep 1
echo -e "\e[1;36m escrevendo em seus cadernos de poesias.  \e[0m"
