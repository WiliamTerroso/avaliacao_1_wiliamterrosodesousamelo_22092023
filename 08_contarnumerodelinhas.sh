#!/bin/bash

# Verificar se foram passados três argumentos
if [ $# -ne 3 ]; then
  echo "Por favor, forneça exatamente três nomes de arquivo como argumentos."
  exit 1
fi

# Capturar os nomes dos arquivos em variáveis
arquivo1="$1"
arquivo2="$2"
arquivo3="$3"

# Usar wc -l para contar o número de linhas de cada arquivo e somá-los
total_linhas=$(($(wc -l < "$arquivo1") + $(wc -l < "$arquivo2") + $(wc -l < "$arquivo3")))

# Imprimir o resultado
echo "A soma do número de linhas dos arquivos $arquivo1, $arquivo2 e $arquivo3 é igual a $total_linhas"

