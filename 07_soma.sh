#!/bin/bash

# Verificar se foram passados três argumentos
if [ $# -ne 3 ]; then
  echo "Por favor, forneça exatamente três números como argumentos."
  exit 1
fi

# Capturar os argumentos em variáveis
num1="$1"
num2="$2"
num3="$3"

# Realizar a soma dos três números
soma=$(echo "$num1 + $num2 + $num3" | bc)

# Imprimir o resultado
echo "A soma de $num1, $num2 e $num3 é igual a $soma"

